import { NgModule} from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';


import {
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatProgressBarModule,
    MatTooltipModule,
    MatDialogModule,
    MatDatepickerModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatSortModule,
    MatTabsModule,
    MatSnackBarModule,
    MatPaginatorModule,
} from '@angular/material';

@NgModule({
      exports: [
        MatButtonModule,
        MatCardModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatTableModule,
        MatProgressBarModule,
        MatTooltipModule,
        MatDialogModule,
        MatDatepickerModule,
        MatListModule,
        MatPaginatorModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatSidenavModule,
        MatSortModule,
        MatMomentDateModule,
        MatTabsModule,
        MatSnackBarModule,
      ]
})

export class MaterialModule {}
