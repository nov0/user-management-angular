export interface Pagination {
    columnName: string;
    order: string;
    query: string;
    pageIndex: number;
    pageNumber: number;
}
