export interface UserData {
    id: string;
    userId: number;
    firstName: string;
    lastName: string;
    birthDate: string;
    imageUrl: string;
    active: boolean;
    address: string;
    phoneNumber: string;
    email: string;
    password: string;
  }
