import { UserData } from './UserData';

export class UserSearchResult {
    numberOfResults: number;
    total: number;
    took: number;
    users: UserData[];
}
