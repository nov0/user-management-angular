import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserData } from '../model/UserData';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Pagination } from '../model/pagination';
import { UserSearchResult } from '../model/user-search-result';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  USER_ENDPOINT = 'http://localhost:8080/user/';

  constructor(private http: HttpClient) {}

  getUsers(pagination: Pagination): Observable<UserSearchResult> {
      return this.http.post<UserSearchResult>(this.USER_ENDPOINT + 'search', pagination);
  }

  getAllUsers(): Observable<UserData[]> {
    return this.http.get<UserData[]>(this.USER_ENDPOINT + 'search/all').pipe(
      catchError(error => {
        console.error('There is no response');
        return of([]);
      })
    );
  }

  saveUser(user: UserData): Observable<UserData> {
    return this.http.post<UserData>(this.USER_ENDPOINT, user);
  }

  updateUser(user: UserData, id: string): Observable<UserData> {
    return this.http.put<UserData>(this.USER_ENDPOINT + '?id=' + id, user);
  }

  getUserById(userId: number): Observable<UserData> {
    return this.http.get<UserData>(this.USER_ENDPOINT + userId);
  }

  deleteUser(id: string): Observable<any> {
    return this.http.delete(this.USER_ENDPOINT + id);
  }

}
