import { DataSource } from '@angular/cdk/table';
import { CollectionViewer } from '@angular/cdk/collections';

import { UserSearchResult } from '../model/user-search-result';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { UserDataService } from './user-data.service';
import { catchError, finalize, tap} from 'rxjs/operators';
import { Pagination } from '../model/pagination';
import { UserData } from '../model/UserData';

export class UserDataSource implements DataSource<UserData> {

    private users = new BehaviorSubject<UserData[]>([]);
    private loadingUsers = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingUsers.asObservable();
    public totalResults: number;
    public took: number;

    constructor(private userService: UserDataService) {}

    connect(): Observable<UserData[]> {
        return this.users.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.users.complete();
        this.loadingUsers.complete();
    }

    getUsers(pagination: Pagination) {
        this.loadingUsers.next(true);
        this.userService.getUsers(pagination)
            .pipe(
                catchError(() => of([])),
                tap((searchResult: UserSearchResult) => {
                    this.totalResults = searchResult.total;
                    this.took = searchResult.took;
                    this.loadingUsers.next(false);
                })
            )
            .subscribe((searchResult: UserSearchResult) => this.users.next(searchResult.users));
    }

}
