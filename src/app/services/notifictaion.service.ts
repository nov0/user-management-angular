import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

    snackbarConfigSuccess: MatSnackBarConfig = {
        duration: 3000,
        verticalPosition: 'top',
        horizontalPosition: 'center'
    };

    snackbarConfigError: MatSnackBarConfig = {
        duration: 6000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: ['snackbar-error']
    };

    constructor(private snackBar: MatSnackBar) {}

    userNewSuccess(): void {
        this.snackBar.open('New user added successfully', 'Dismiss', this.snackbarConfigSuccess);
    }

    userNewError(): void {
        this.snackBar.open('Error while adding new user!!!', 'Dismiss', this.snackbarConfigError);
    }

    userEditSuccess(): void {
        this.snackBar.open('User data edited successfully', 'Dismiss', this.snackbarConfigSuccess);
    }

    userEditError(): void {
        this.snackBar.open('Error while editing user data!!!', 'Dismiss', this.snackbarConfigError);
    }

    userDeleteSuccess(): void {
        this.snackBar.open('User deleted successfully', 'Dismiss', this.snackbarConfigSuccess);
    }

    userDeleteError(): void {
        this.snackBar.open('Error while deleting user!!!', 'Dismiss', this.snackbarConfigError);
    }

}
