/** imports */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './modules/material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/** services */
import { UserDataService } from './services/user-data.service';
import { NotificationService } from './services/notifictaion.service';

/** components */
import { UserListComponent } from './components/user-list/user-list.component';
import { UserDialogComponent } from './components/user-dialog/user-dialog.component';
import { DeleteUserDialogComponent } from './components/delete-user-dialog/delete-user-dialog.component';
import { AboutDialogComponent } from './components/about-dialog/about-dialog.component';


@NgModule({

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],

  declarations: [
    AppComponent,
    UserListComponent,
    UserDialogComponent,
    DeleteUserDialogComponent,
    AboutDialogComponent
  ],

  providers: [ UserDataService, NotificationService],

  bootstrap: [AppComponent],

  entryComponents: [
    UserDialogComponent,
    DeleteUserDialogComponent,
    AboutDialogComponent
  ]
})

export class AppModule { }
