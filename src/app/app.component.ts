import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AboutDialogComponent } from './components/about-dialog/about-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'user-management';

  constructor(private dialog: MatDialog) {}

  openAboutDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';
    const dialogRef = this.dialog.open(AboutDialogComponent, dialogConfig);
  }
}
