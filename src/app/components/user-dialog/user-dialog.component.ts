import { Component, OnInit, Inject, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import * as moment from 'moment';
import { UserDataService } from './../../services/user-data.service';
import { UserData } from './../../model/UserData';
import { NotificationService } from './../../services/notifictaion.service';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css']
})
export class UserDialogComponent implements OnInit {

  form: FormGroup;
  title: string;
  isSaving: boolean;
  isInitialisingForm: boolean;
  id: string;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UserDialogComponent>,
    private userDataService: UserDataService,
    private notificationService: NotificationService,
    @Inject(MAT_DIALOG_DATA) data) {
      this.title = data.title;
      this.id = data.id;
      this.initializeForm(data, formBuilder);
      if (data.userId != null) {
        this.getUser(data);
      }
  }

  ngOnInit() {
  }

  getUser(data: UserData) {
    if (data.userId !== null) {
      this.isInitialisingForm = true;
      // here id is overriden. if you editing please save old id for elastic
      this.userDataService.getUserById(data.userId)
        .subscribe((userData: UserData) => {
          data = userData;
          this.form.patchValue(userData);
          this.isInitialisingForm = false;
        }, error =>
            this.isInitialisingForm = false);
    }
  }

  initializeForm(data: UserData, formBuilder: any) {
    this.form = formBuilder.group({
      id: [data.id],
      userId: [data.userId],
      firstName: [data.firstName, Validators.required],
      lastName: [data.lastName, Validators.required],
      email: [data.email, Validators.required],
      password: [data.password, Validators.required],
      birthDate: [moment(), Validators.required],
      address: [data.address],
      imageUrl: [data.imageUrl],
      phoneNumber: [data.phoneNumber],
  });
  }

  save() {
    this.isSaving = true;
    // editing user
    if (this.form.value.userId != null && this.id != null) {
      this.userDataService.updateUser(this.form.value, this.id)
      .subscribe(data => {
        this.notificationService.userEditSuccess();
        this.id = null;
        this.isSaving = false;
        this.dialogRef.close({status: 200});
      }, error => {
        this.isSaving = false;
        this.notificationService.userEditError();
      });
    } else {
      // adding new user
      this.userDataService.saveUser(this.form.value)
      .subscribe(data => {
        this.notificationService.userNewSuccess();
        this.isSaving = false;
        this.dialogRef.close({status: 200});
      }, error => {
        this.isSaving = false;
        this.notificationService.userNewError();
      });
    }
  }

  close() {
    this.dialogRef.close({status: 304});
  }
}
