import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserDataService } from './../../services/user-data.service';
import { NotificationService } from './../../services/notifictaion.service';

@Component({
  selector: 'app-delete-user-dialog',
  templateUrl: './delete-user-dialog.component.html',
  styleUrls: ['./delete-user-dialog.component.css']
})
export class DeleteUserDialogComponent implements OnInit {

  user: { id: string; firstName: string; lastName: string };

  constructor(private dialogRef: MatDialogRef<DeleteUserDialogComponent>,
              private userDataService: UserDataService,
              private notificationService: NotificationService,
              @Inject(MAT_DIALOG_DATA) private data) {
                this.user = data;
              }

  ngOnInit() {}

  delete(): void {
    if (this.user.id !== null) {
        this.userDataService.deleteUser(this.user.id)
        .subscribe(data => {
          this.notificationService.userDeleteSuccess();
          this.dialogRef.close({status: 200});
        }, error => {
          this.notificationService.userDeleteError();
          this.dialogRef.close({status: 500});
          });
    }
  }

  cancel(): void {
    this.dialogRef.close({status: 304});
  }

}
