import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-about-dialog',
  templateUrl: './about-dialog.component.html',
  styleUrls: ['./about-dialog.component.css']
})
export class AboutDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<AboutDialogComponent>) { }

  ngOnInit() {
  }

  cancel(): void {
    this.dialogRef.close({status: 304});
  }

}
