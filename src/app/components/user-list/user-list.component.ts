import { Component, ViewChild, AfterViewInit , OnInit, ElementRef} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { UserDataService } from './../../services/user-data.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { UserDialogComponent } from './../user-dialog/user-dialog.component';
import { DeleteUserDialogComponent } from './../delete-user-dialog/delete-user-dialog.component';
import { UserDataSource } from 'src/app/services/user-data-source';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { merge, fromEvent } from 'rxjs';
import { Pagination } from 'src/app/model/pagination';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements AfterViewInit, OnInit {

  displayedColumns: string[] = ['firstName', 'lastName', 'birthDate', 'active', 'action'];
  dataSource: UserDataSource;

  lastDeletedItem: string;
  loadingData: boolean;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filter', {static: true}) input: ElementRef;

  constructor(
    private userDataService: UserDataService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.dataSource = new UserDataSource(this.userDataService);
    this.loadUserData();
  }

  ngAfterViewInit(): void {

    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
          debounceTime(300),
          distinctUntilChanged(),
          tap(() => {
              this.paginator.pageIndex = 0;
              this.loadUserData();
          })
      )
    .subscribe();

     // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(_ => this.loadUserData())
      )
      .subscribe();
  }

  loadUserData() {
    this.dataSource.getUsers(this.getPagination());
  }

  private getPagination(): Pagination {
    const pagination: Pagination = {
      columnName: this.sort && this.sort.active ? this.sort.active : 'firstName',
      order: this.sort && this.sort.direction ? this.sort.direction : 'asc',
      query: this.input.nativeElement.value,
      pageIndex: this.paginator && this.paginator.pageIndex ? this.paginator.pageIndex : 0,
      pageNumber: this.paginator && this.paginator.pageSize ? this.paginator.pageSize : 50
    };
    return pagination;
  }

  wait(miliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, miliseconds));
  }

  openUserDialog(id: string , userId: number) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '600px';
    dialogConfig.data = {
      id: id || null,
      userId: userId || null,
      title: id ? 'Edit user dialog' : 'New User dialog'
    };
    const dialogRef = this.dialog.open(UserDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data.status === 200) {
          setTimeout(() => this.loadUserData(), 300);
        }
      },
    );
  }

  openDeleteDialog(id: string, firstName: string, lastName: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.width = '350px';
    dialogConfig.data = { id, firstName, lastName };
    const dialogRef = this.dialog.open(DeleteUserDialogComponent, dialogConfig);
    dialogRef.afterClosed()
      .subscribe(data => {
        if (data.status === 200) {
          setTimeout(() => {
            this.lastDeletedItem = id;
            this.loadUserData();
          }, 300);
        }
      }
    );
  }

  onRowClick(row: any): void {
    console.log('Row clicked: ' + JSON.stringify(row));
  }

}
